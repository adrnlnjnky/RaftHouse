package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type GuideShack interface {
	DaysWorked(guide string) int
	RecordDay(guide string)
	GetTeam() Team
}

type Guide struct {
	Name string
	Days int
}

type GuideServer struct {
	shack GuideShack
	http.Handler
}

const jsonContentType = "application/json"

func NewGuideServer(shack GuideShack) *GuideServer {
	g := new(GuideServer)

	g.shack = shack

	router := http.NewServeMux()
	router.Handle("/team", http.HandlerFunc(g.teamHandler))
	router.Handle("/guides/", http.HandlerFunc(g.guideHandler))

	g.Handler = router

	return g
}

func (g *GuideServer) teamHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", jsonContentType)
	json.NewEncoder(w).Encode(g.shack.GetTeam)
}

func (g *GuideServer) guideHandler(w http.ResponseWriter, r *http.Request) {
	guide := r.URL.Path[len("/guides/"):]

	switch r.Method {
	case http.MethodPost:
		g.recordDay(w, guide)
	case http.MethodGet:
		g.daysWorked(w, guide)
	}
}

func (g *GuideServer) daysWorked(w http.ResponseWriter, guide string) {
	days := g.shack.DaysWorked(guide)

	if days == 0 {
		w.WriteHeader(http.StatusNotFound)
	}
	fmt.Fprint(w, days)
}

func (g *GuideServer) recordDay(w http.ResponseWriter, guide string) {
	g.shack.RecordDay(guide)
	w.WriteHeader(http.StatusAccepted)
}
