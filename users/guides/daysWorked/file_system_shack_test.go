package main

import (
	"io"
	"io/ioutil"
	"os"
	"testing"
)

func createTempFile(t *testing.T, initialData string) (io.ReadWriteSeeker, func()) {
	t.Helper()

	tmpfile, err := ioutil.TempFile("", "db")

	if err != nil {
		t.Fatalf("temp file not created: %v", err)
	}
	tmpfile.Write([]byte(initialData))

	removeFile := func() {
		tmpfile.Close()
		os.Remove(tmpfile.Name())
	}
	return tmpfile, removeFile
}

func TestFileSystemShack(t *testing.T) {

	t.Run("Get Team Info from a reader", func(t *testing.T) {
		database, cleanDatabase := createTempFile(t, `[
			{"Name": "Darby", "Days": 21},
			{"Name": "Brown", "Days": 33}]`)
		defer cleanDatabase()

		shack := &FileSystemGuideShack{database}

		got := shack.GetTeam()

		want := []Guide{
			{"Darby", 21},
			{"Brown", 33},
		}
		assertTeam(t, got, want)

		//read it a second time
		got = shack.GetTeam()
		assertTeam(t, got, want)
	})

	t.Run("Get Guide's Days", func(t *testing.T) {
		database, cleanDatabase := createTempFile(t, `[
			{"Name": "Darby", "Days": 21},
			{"Name": "Brown", "Days": 33}]`)
		defer cleanDatabase()

		shack := FileSystemGuideShack{database}

		got := shack.GetGuideDays("Brown")
		want := 33
		assertDaysEqual(t, got, want)
	})

	t.Run("Store days for Guides", func(t *testing.T) {
		database, cleanDatabase := createTempFile(t, `[
			{"Name": "Darby", "Days": 21},
			{"Name": "Brown", "Days": 33}]`)
		defer cleanDatabase()

		shack := FileSystemGuideShack{database}

		shack.RecordDay("Brown")

		got := shack.GetGuideDays("Brown")
		want := 34
		assertDaysEqual(t, got, want)
	})

}

func assertDaysEqual(t *testing.T, got, want int) {
	t.Helper()
	if got != want {
		t.Errorf("got %d want %d", got, want)
	}

}
