package main

import (
	"encoding/json"
	"fmt"
	"os"
	"sort"
)

// FileSystemGuideShack
type FileSystemGuideShack struct {
	database *json.Encoder
	team     Team
}

func NewFileSystemGuideShack(file *os.File) (*FileSystemGuideShack, error) {
	err := initialiseGuideDBFile(file)
	if err != nil {
		return nil, fmt.Errorf("problem initializing Guide db file, %v", err)
	}
	team, err := NewTeam(file)
	if err != nil {
		return nil, fmt.Errorf("problem loading guide info from file %s, %v", file.Name(), err)
	}
	return &FileSystemGuideShack{
		database: json.NewEncoder(&tape{file}),
		team:     team,
	}, nil
}

func initialiseGuideDBFile(file *os.File) error {
	file.Seek(0, 0)

	info, err := file.Stat()

	if err != nil {
		return fmt.Errorf("Problem getting file info from file %s, %v error", file.Name(), err)
	}

	if info.Size() == 0 {
		file.Write([]byte("[]"))
		file.Seek(0, 0)
	}
	return nil
}

func (f *FileSystemGuideShack) GetTeam() []Guide {
	sort.Slice(f.team, func(i, j int) bool {
		return f.team[i].Days > f.team[j].Days
	})
	return f.team
}

func (f *FileSystemGuideShack) GetGuideDays(name string) int {
	guide := f.team.Find(name)
	if guide != nil {
		return guide.Days
	}
	return 0
}

func (f *FileSystemGuideShack) RecordDay(name string) {
	guide := f.team.Find(name)
	if guide != nil {
		guide.Days++
	} else {
		f.team = append(f.team, Guide{name, 1})
	}
	f.database.Encode(f.team)
}
