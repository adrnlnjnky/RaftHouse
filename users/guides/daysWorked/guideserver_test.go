package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

type StubGuideShack struct {
	days     map[string]int
	dayCalls []string
	team     Team
}

// func (s *StubGuideShack) GetTeam(Name string) {
// 	s.team
// }

func (s *StubGuideShack) DaysWorked(guide string) int {
	days := s.days[guide]
	return days
}

func (s *StubGuideShack) RecordDay(guide string) {
	s.dayCalls = append(s.dayCalls, guide)
}

func TestTeam(t *testing.T) {
	// shack := StubGuideShack{}
	// server := NewGuideServer(&shack)

	t.Run("Returns 200 on /team", func(t *testing.T) {
		request, _ := http.NewRequest(http.MethodGet, "/team", nil)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		var got []Guide

		err := json.NewDecoder(response.Body).Decode(&got)

		if err != nil {
			t.Fatalf("Player %d not parsed from server %d", err, response.Body)
		}
		assertStatus(t, response.Code, http.StatusOK)
	})
}

func getTeamFromResponse(t *testing.T, body io.Reader) []Guide {
	t.Helper()
	team, err := NewTeam(body)

	if err != nil {
		t.Fatalf("Unable to parse response from server %q into slice of Guide, '%v'", body, err)
	}
	return team
}

func newGetDaysWorked(name string) *http.Request {
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/guides/%s", name), nil)
	return req
}

func TestRecordDays(t *testing.T) {
	shack := StubGuideShack{
		map[string]int{},
		nil,
	}
	server := NewGuideServer(&shack)

	t.Run("Records win on Post", func(t *testing.T) {
		guide := "Logan"

		request := newPostDayRequest(guide)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		assertStatus(t, response.Code, http.StatusAccepted)

		if len(shack.dayCalls) != 1 {
			t.Errorf("got %d calls to Record Day: want 1", len(shack.dayCalls))
		}
		if shack.dayCalls[0] != guide {
			t.Errorf("Recorded %q, Wanted: %q", shack.dayCalls[0], guide)
		}
	})

}

func newPostDayRequest(guide string) *http.Request {
	req, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("/guides/%s", guide), nil)
	return req
}

func TestGuideServer(t *testing.T) {
	shack := StubGuideShack{
		map[string]int{
			"MadDog": 20,
			"Dakota": 10,
		},
		nil,
	}
	server := NewGuideServer(&shack)

	t.Run("Returns MadDog's Days Worked", func(t *testing.T) {
		request := newGetDaysWorked("MadDog")
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		assertResponseBody(t, response.Body.String(), "20")
		assertStatus(t, response.Code, http.StatusOK)
	})

	t.Run("Returns Dakota's Days Worked", func(t *testing.T) {
		request := newGetDaysWorked("Dakota")
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		assertResponseBody(t, response.Body.String(), "10")
		assertStatus(t, response.Code, http.StatusOK)
	})

	t.Run("Guide does not exist", func(t *testing.T) {
		request := newGetDaysWorked("Apallo")
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		got := response.Code
		want := http.StatusNotFound

		if got != want {
			t.Errorf("They should not exits")
		}
	})

}

func assertStatus(t *testing.T, got, want int) {
	t.Helper()
	if got != want {
		t.Errorf("did not get correct status: got %d but want %d", got, want)
	}
}

func assertResponseBody(t *testing.T, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("Response is wrong!! Wanted: %q and got %q instead!!!!!!!", got, want)
	}
}

func assertTeam(t *testing.T, got, want []Guide) {
	t.Helper()
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v and wanted %v", got, want)

	}
}
