package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRecordingDaysAndRetrievingThem(t *testing.T) {
	shack := NewInMemoryGuideShack()
	server := NewGuideServer(shack)
	guide := "MadDog"

	server.ServeHTTP(httptest.NewRecorder(), newPostDayRequest(guide))
	server.ServeHTTP(httptest.NewRecorder(), newPostDayRequest(guide))
	server.ServeHTTP(httptest.NewRecorder(), newPostDayRequest(guide))

	response := httptest.NewRecorder()
	server.ServeHTTP(response, newGetDaysWorked(guide))
	assertStatus(t, response.Code, http.StatusOK)

	assertResponseBody(t, response.Body.String(), "3")
}
