package main

import (
	"log"
	"net/http"
	"os"
)

const dbFileName = "days.db.json"

func main() {
	db, err := os.OpenFile(dbFileName, os.O_RDWR|os.O_CREATE, 0666)

	if err != nil {
		log.Fatalf("Problem opening %s, error: %v", dbFileName, err)
	}

	shack, err := NewFileSystemGuideShack(db)

	if err != nil {
		log.Fatalf("problem creating file system guide shack, %v ", err)
	}

	server := NewGuideServer(shack)

	if err := http.ListenAndServe(":1991", server); err != nil {
		log.Fatalf("could not listen to port 1991 %v", err)
	}
}
