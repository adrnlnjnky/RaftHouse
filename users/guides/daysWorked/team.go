package main

import (
	"encoding/json"
	"fmt"
	"io"
)

type Team []Guide

func (t Team) Find(name string) *Guide {
	for i, p := range t {
		if p.Name == name {
			return &t[i]
		}
	}
	return nil
}

func NewTeam(rdr io.Reader) ([]Guide, error) {
	var team []Guide
	err := json.NewDecoder(rdr).Decode(&team)
	if err != nil {
		err = fmt.Errorf("problem parsing team, %v", err)
	}
	return team, err
}
