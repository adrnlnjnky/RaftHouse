package guides

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"gitlab.com/adrnlnjnky/RaftHouse/dbaccess"
	"gitlab.com/adrnlnjnky/RaftHouse/employee"
	"gitlab.com/adrnlnjnky/RaftHouse/handlers"
)

// GuideInfo interface to access the guides basic info.
type GuideInfo interface {
	ListGuides() []Guide
	IsGuide(ctx gin.Context)
	AddGuide(ctx gin.Context)
}

//Guide is a guide for the company.
type Guide struct {
	gorm.Model
	Handle     string
	Phone      string
	Email      string
	Active     bool
	ClassIII   bool
	ClassIV    bool
	ClassV     bool
	Bike       bool
	Climb      bool
	RopesHigh  bool
	RopesLow   bool
	EmployeeID employee.EmployeeGeneral
}

// ListGuides will list all guides
func (g *Guide) ListGuides() {
	// guides := g.name
	guides := []Guide{}

	for i := range guides {
		c := guides[i]
		fmt.Println(c)
		fmt.Println("Guide:", c)
	}
}

// IsGuide returns true or false if a person is a guide.
// GetGuide will return a guide I I mess all sortsa stuff up
func (g *Guide) GetGuide(ctx *gin.Context) bool {
	db := setupDB()
	search := ctx.Param("search")
	column, regsearch := handlers.SortSearch(search)
	target := fmt.Sprintf("%v LIKE ?", column)
	var guide []Guide
	err := db.Debug().Model(&Guide{}).Where(target, regsearch).Find(&guide)
	switch {
	case err.Error != nil:
		handlers.DbError(ctx, err.Error)
	case err.RowsAffected == 0:
		handlers.NoRows(ctx)
	default:
		ctx.JSON(200, target)
	}
	return false
}

// AddGuide adds a guide to the system
func (g *Guide) AddGuide(ctx *gin.Context) {
	guide := Guide{}
	ctx.BindJSON(&guide)

}

// This calls out to open the database then makes the link with the table.
func setupDB() *gorm.DB {
	db := dbaccess.GormOpenDB()
	db.AutoMigrate(&Guide{})
	return db
}
