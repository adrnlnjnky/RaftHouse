package guides

import (
	"testing"
)

func TestGuide(t *testing.T) {
	t.Run("Guide exists", func(t *testing.T) {
		guides := Guide{}
		name := "Paige"
		guide := guides.IsGuide(name)
		got := guide
		want := true
		if got != want {
			t.Errorf("got %v want %v Oh Yeah and HAhA sucker!!", got, want)
		}
	})

	t.Run("Guide does notexists", func(t *testing.T) {
		guides := Guide{}
		name := "Batman"

		guide := guides.IsGuide(name)

		got := guide
		want := false

		if got != want {
			t.Errorf("got %v want %v Oh Yeah and HAhA sucker!!", got, want)
		}
	})

	// t.Run("Guide is Class III", func(t *testing.T) {
	// 	guides := Guides{}
	// 	name := guides.name
	// 	classIII := guides.name.classIII()

	// 	got := classIII
	// 	want := true

	// 	if got != want {
	// 		t.Errorf("got %v want %v", got, want)
	// 	}
	// })
}
