package employee

// EmployeeGeneral is struct of all general information for our employees.
type EmployeeGeneral struct {
	Handle      string
	Prefix      string
	FirstName   string
	MiddleName  string
	LastName    string
	Suffix      string
	IsDriverBus bool
	IsDriverVan bool
	IsGuide     bool
	GuideInfo   guide.Guide
}

// EmployeeSecrets are the sensitive bits we keep on employees.  I want to keep these separate from everything else.
// Once these things are in the database they never need to be pulled into memmory unless we are useing them.
type EmployeeSecrets struct {
	SSN      string
	Passport string
	DriveLic string
	DLState  string
}
