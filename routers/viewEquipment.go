package router

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
)

var (
	d = time.Date
	h = time.Hour
	m = time.Minute
	t = fmt.Sprintf("%s:%s", h, m)
)

// ViewEquip is a quick easy view for equipment availability
func ViewEquip(ctx *gin.Context) {
	titles := gin.H{
		"paddles":       "Paddles",
		"throwbags":     "Throwbags",
		"dittybags":     "Ditty Bags",
		"waterbottles":  "Water Bottles",
		"wrapkits":      "Wrap Kits",
		"pumps":         "Pumps",
		"spare":         "Spare LifeJ & Helmet",
		"day":           d,
		"time":          t,
		"paddlesA":      paddles.Available(),
		"throwbagsA":    throwbags.Available(),
		"dittybagsA":    dittybags.Available(),
		"waterbottlesA": waterBottles.Available(),
		"wrapkitsA":     wrapKits.Available(),
		"pumpsA":        pumps.Available(),
		"spareA":        spareJandH.Available(),
	}
	ctx.HTML(200, "index.tmpl", titles)
}
