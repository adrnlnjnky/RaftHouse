package router

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/adrnlnjnky/RaftHouse/warehouse/equipment"
)

var (
	paddles      = equipment.Paddles{}
	throwbags    = equipment.ThrowBags{}
	dittybags    = equipment.DittyBags{}
	waterBottles = equipment.WaterBottles{}
	wrapKits     = equipment.WrapKits{}
	pumps        = equipment.Pump{}
	spareJandH   = equipment.SpareJandH{}
	trailer      = equipment.TrailerBasket{}
)

//CheckOut will return equipment to the available inventory
func CheckOut(ctx *gin.Context) {
	left := equipment.TrailerBasket{}.Paddles
	ctx.SetCookie("GearHolder", "54", 12, "cookies/gotgear", "255.255.255.255", true, true)
	var data map[string]equipment.Quantity
	ctx.ShouldBindJSON(&data)
	for gear, quantity := range data {
		fmt.Println("GEAR", gear)
		switch {
		case gear == "paddles":
			paddles.CheckingOut(quantity)
			trailer.AddPaddlesToTrailer(quantity)
			confirm := fmt.Sprintf("You Took %v, %s: %s in trailer.", quantity, gear, left)
			ctx.JSON(200, confirm)
			continue
		case gear == "throwbags":
			throwbags.CheckingOut(quantity)
			trailer.AddThrowBagsToTrailer(quantity)
			confirm := fmt.Sprintf("You Took %v, %s", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "dittybags":
			dittybags.CheckingOut(quantity)
			trailer.AddDittyBagsToTrailer(quantity)
			confirm := fmt.Sprintf("You Took %v, %s", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "waterbottles":
			waterBottles.CheckingOut(quantity)
			trailer.AddWaterBottlesToTrailer(quantity)
			confirm := fmt.Sprintf("You Took %v, %s", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "wrapkits":
			wrapKits.CheckingOut(quantity)
			trailer.AddWrapKitsToTrailer(quantity)
			confirm := fmt.Sprintf("You Took %v, %s", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "pumps":
			pumps.CheckingOut(quantity)
			trailer.AddPumpToTrailer(quantity)
			confirm := fmt.Sprintf("You Took %v, %s", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "Spares":
			spareJandH.CheckingOut(quantity)
			trailer.AddSpareJandHToTrailer(quantity)
			confirm := fmt.Sprintf("You Took %v, %s", quantity, gear)
			ctx.JSON(200, confirm)

		}
	}
}

//CheckIn will return equipment to the available inventory
func CheckIn(ctx *gin.Context) {
	var data map[string]equipment.Quantity
	ctx.ShouldBindJSON(&data)
	for gear, quantity := range data {
		switch {
		case gear == "paddles":
			paddles.Returning(quantity)
			trailer.RemovePaddlesFromTrailer(quantity)
			left := equipment.TrailerBasket{}.Paddles
			err := trailer.RemovePaddlesFromTrailer(quantity)
			if err != nil {
				note := fmt.Sprintf("Error Returning %s: Error: %s : %s in trailer", gear, err, left)
				ctx.JSON(404, note)
				return
			}
			confirm := fmt.Sprintf("You Returned %v, %s: %s Left in Trailer", quantity, gear, left)
			ctx.JSON(200, confirm)
		case gear == "throwbag":
			throwbags.Returning(quantity)
			trailer.RemoveThrowBagsFromTrailer(quantity)
			confirm := fmt.Sprintf("You Returned %v, %s", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "dittybags":
			dittybags.Returning(quantity)
			trailer.RemoveDittyBagsFromTrailer(quantity)
			confirm := fmt.Sprintf("You Took %v, %s", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "waterbottles":
			waterBottles.Returning(quantity)
			trailer.RemoveWaterBottlesFromTrailer(quantity)
			confirm := fmt.Sprintf("You Took %v, %s", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "wrapkits":
			wrapKits.Returning(quantity)
			trailer.RemoveWrapKitsFromTrailer(quantity)
			confirm := fmt.Sprintf("You Took %v, %s", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "pumps":
			pumps.Returning(quantity)
			trailer.RemovePumpFromTrailer(quantity)
			confirm := fmt.Sprintf("You Took %v, %s", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "Spares":
			spareJandH.Returning(quantity)
			trailer.RemoveSpareJandHFromTrailer(quantity)
			confirm := fmt.Sprintf("You Took %v, %s", quantity, gear)
			ctx.JSON(200, confirm)
		}
	}
}

// Remove will remove equipment to inventory
func Remove(ctx *gin.Context) {
	var data map[string]equipment.Quantity
	ctx.ShouldBindJSON(&data)
	for gear, quantity := range data {
		switch {
		case gear == "paddles":
			paddles.CheckingOut(quantity)
			confirm := fmt.Sprintf("You are removing %v, %s from Inventory", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "throwbags":
			throwbags.CheckingOut(quantity)
			confirm := fmt.Sprintf("You are removing %v, %s from Inventory", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "dittybags":
			dittybags.CheckingOut(quantity)
			confirm := fmt.Sprintf("You are removing %v, %s from Inventory", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "waterbottles":
			waterBottles.CheckingOut(quantity)
			confirm := fmt.Sprintf("You are removing %v, %s from Inventory", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "wrapkits":
			wrapKits.CheckingOut(quantity)
			confirm := fmt.Sprintf("You are removing %v, %s from Inventory", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "pumps":
			pumps.CheckingOut(quantity)
			confirm := fmt.Sprintf("You are removing %v, %s from Inventory", quantity, gear)
			ctx.JSON(200, confirm)
		case gear == "Spares":
			spareJandH.CheckingOut(quantity)
			confirm := fmt.Sprintf("You are removing %v, %s from Inventory", quantity, gear)
			ctx.JSON(200, confirm)

		}
	}
}

// Add will add equipment to inventory
func Add(ctx *gin.Context) {
	var data map[string]equipment.Quantity
	ctx.ShouldBindJSON(&data)
	for gear, quantity := range data {
		switch {
		case gear == "paddles":
			paddles.Returning(quantity)
			confirm := (fmt.Sprintf("You Added %v %s to inventory ", quantity, gear))
			ctx.JSON(200, confirm)
			continue
		case gear == "throwbags":
			throwbags.Returning(quantity)
			confirm := (fmt.Sprintf("You Added %v %s to inventory ", quantity, gear))
			ctx.JSON(200, confirm)
			continue
		case gear == "dittybags":
			dittybags.Returning(quantity)
			confirm := (fmt.Sprintf("You Added %v %s to inventory ", quantity, gear))
			ctx.JSON(200, confirm)
			continue
		case gear == "waterbottles":
			waterBottles.Returning(quantity)
			confirm := (fmt.Sprintf("You Added %v %s to inventory ", quantity, gear))
			ctx.JSON(200, confirm)
			continue
		case gear == "wrapkits":
			wrapKits.Returning(quantity)
			confirm := (fmt.Sprintf("You Added %v %s to inventory ", quantity, gear))
			ctx.JSON(200, confirm)
			continue
		case gear == "pumps":
			pumps.Returning(quantity)
			confirm := (fmt.Sprintf("You Added %v %s to inventory ", quantity, gear))
			ctx.JSON(200, confirm)
			continue
		case gear == "spares":
			confirm := (fmt.Sprintf("You Added %v %s to inventory ", quantity, gear))
			ctx.JSON(200, confirm)
		}
	}
}

// View shows available equipment in the warehouse.
func View(ctx *gin.Context) {
	equip := ctx.Param("search")

	switch {
	case equip == "paddles":
		available := paddles.Available()
		ctx.JSON(200, available)
	case equip == "throwbags":
		available := throwbags.Available()
		ctx.JSON(200, available)
	case equip == "dittybags":
		available := dittybags.Available()
		ctx.JSON(200, available)
	case equip == "waterbottles":
		available := waterBottles.Available()
		ctx.JSON(200, available)
	case equip == "wrapkits":
		available := wrapKits.Available()
		ctx.JSON(200, available)
	case equip == "pumps":
		available := pumps.Available()
		ctx.JSON(200, available)
	case equip == "spare":
		available := spareJandH.Available()
		ctx.JSON(200, available)
	default:
		ctx.JSON(http.StatusNotFound, "We don't have that")
	}

}
