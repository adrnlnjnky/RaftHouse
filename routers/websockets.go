package router

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"gitlab.com/adrnlnjnky/RaftHouse/warehouse/equipment"
)

var wsupgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func wshandler(w http.ResponseWriter, r *http.Request) {
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("Failed to set websocket upgrade: %+v", err)
		return
	}
	for {
		t, msg, err := conn.ReadMessage()
		if err != nil {
			log.Printf("connection Read Error: %+v", err)
			break
		}
		conn.WriteMessage(t, msg)
	}
}

// PushStuff is going to push some data to some boxes.
func PushStuff(w http.ResponseWriter, r *http.Request) {
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("Failed to set websocket upgrade: %+v", err)
		return
	}
	for {
		temp := fmt.Sprintf("%d", paddles.Available())
		JSONblob, _ := json.Marshal(temp)
		err = conn.WriteMessage(websocket.TextMessage, JSONblob)
		if err != nil {
			log.Println(err)
			break
		}
		time.Sleep(1 * time.Second)
	}
	conn.Close()
	fmt.Println("Client unsubscribed")
}

func wsSetup(ctx *gin.Context) {
	w := ctx.Writer
	r := ctx.Request
	wsPaddles(w, r)

	ctx.HTML(http.StatusOK, "index.tmpl", wsPaddles)
}

func wsPaddles(w http.ResponseWriter, r *http.Request) {
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err)
	}

	for {
		paddles := equipment.Paddles{}
		var info []byte
		info, err := json.Marshal(paddles)
		if err != nil {
			log.Println(err)
			return
		}
		err = conn.WriteMessage(websocket.TextMessage, info)
		if err != nil {
			log.Println(err)
			break
		}
	}
}
