package dbaccess

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// GormOpenDB opens the database
func GormOpenDB() *gorm.DB {
	dsn := "user=postgres dbname=DBASE port=PORT sslmode=MODE"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to open database")
	}
	return db
}

// Opendb opens the db without an ORM
func Opendb(dbase string) *sql.DB {
	file := dbase
	err := godotenv.Load(file)
	if err != nil {
		fmt.Println("Shart")
		log.Fatalf("Error loading %v file", dbase)
	}
	db, err := sql.Open("postgres", os.Getenv("POSTGRES_URL"))
	if err != nil {
		fmt.Println("Farts")
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		fmt.Println("Lumpy Farts")
		panic(err)
	}
	return db
}
