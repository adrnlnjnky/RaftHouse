package helpers

import (
	"fmt"
	"log"
	"os"
	"reflect"

	"github.com/joho/godotenv"
)

func openEnv(env string) error {
	e := godotenv.Load(env)
	if e != nil {
		log.Fatal("Error loading environment")
	}
	fmt.Println("E is of type:", reflect.TypeOf(e))
	return e
}

// SetupServerEnv Returns port and router information for the server to use.
func SetupServerEnv(env string) (string, string) {
	openEnv(env)
	port := os.Getenv("PORT")
	route := os.Getenv("ROUTE")
	return port, route
}
