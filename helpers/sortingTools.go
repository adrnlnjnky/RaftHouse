package helpers

import (
	"net"
	"regexp"
	"strings"
)

var (
	emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	phoneRegex = regexp.MustCompile(`^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$`)
)

// SortSearch reaches out to isEmail, and isPhone and checks to see if they
// qualify.  first true answer wins.
func SortSearch(data string) (string, string) {
	reg := "%" + data + "%"

	isPhone := isPhone(data)
	if isPhone == true {
		return "phone", reg
	}

	isEmail := isEmail(data)
	if isEmail == true {
		parts := strings.Split(data, "@")
		regemail := "%" + parts[0] + "%@" + parts[1]
		return "email", regemail
	}

	return "name", reg
}

func isPhone(data string) bool {
	if len(data) < 3 || len(data) > 11 {
		return false
	}
	if !phoneRegex.MatchString(data) {
		return false
	}
	return true
}

// IsPhoneValid checks that the phone number meets regex requirements
func IsPhoneValid(data string) bool {
	if len(data) < 10 || len(data) > 20 {
		return false
	}
	if !phoneRegex.MatchString(data) {
		return false
	}
	return true
}

func isEmail(data string) bool {
	if emailRegex.MatchString(data) {
		return true
	}
	var parts []string
	parts = strings.SplitAfter(data, "@")
	if len(parts) == 2 {
		return true
	}
	return false
}

// IsEmailValid checks if the email provided passes the required structure
// and length test. It also checks the domain has a valid MX record.
func IsEmailValid(e string) bool {
	if len(e) < 3 && len(e) > 254 {
		return false
	}
	if !emailRegex.MatchString(e) {
		return false
	}
	parts := strings.Split(e, "@")
	mx, err := net.LookupMX(parts[1])
	if err != nil || len(mx) == 0 {
		return false
	}
	return true
}
