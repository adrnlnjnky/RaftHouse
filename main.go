package main

import (
	logg "gitlab.com/adrnlnjnky/RaftHouse/logging"
	"gitlab.com/adrnlnjnky/RaftHouse/servers"
)

// We are going to set up the logging and then call the web service.
func main() {
	logg.SetupLogOutput()
	// db := dbaccess.GormOpenDB()
	servers.Central()
}
