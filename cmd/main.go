package main

import (
	"fmt"

	"gitlab.com/adrnlnjnky/RaftHouse/guides"
	"gitlab.com/adrnlnjnky/RaftHouse/transport"
	"gitlab.com/adrnlnjnky/RaftHouse/warehouse"
)

// "fmt"

func main() {

	//	checking out := "fugitive"

	// riverGear := warehouse.RiverGear{
	// 	Paddles:   20,
	// 	ThrowBags: 20,
	// }
	paddles := warehouse.Paddles{

		InWarehouse: 250,
	}
	throwBags := warehouse.ThrowBags{
		InWarehouse: 40,
	}
	basket := transport.TrailerBasket{
		Paddles:      0,
		Throwbags:    0,
		DittyBags:    0,
		WaterBottles: 0,
		Pump:         0,
		SpareJandH:   0,
	}

	fmt.Println("Paddles In Warehouse: ", paddles.Available())
	fmt.Println("Paddles in the trailer", basket.PaddlesInTrailer())

	LoadPaddles(25)
	paddles.CheckingOut(20)
	basket.AddPaddlesToTrailer(20)

	fmt.Println("I just added 20 paddles to the trailer")
	fmt.Println("Paddles In Warehouse: ", paddles.Available())
	fmt.Println("Paddles in the trailer", basket.PaddlesInTrailer())
	fmt.Println("Throwbags Available:", throwBags.InWarehouse)
	fmt.Println()

	// guides := guides.Guides

	Darby := guides.Guide{
		Name:      "'Old man River' Darby",
		Active:    true,
		Class_III: true,
		Class_IV:  true,
		Class_V:   true,
		Bike:      true,
		Climb:     false,
		RopesHigh: false,
		RopesLow:  false,
	}

	Paige := guides.Guide{
		Name:      "Rage Paige",
		Active:    true,
		Class_III: true,
		Class_IV:  true,
		Class_V:   false,
		Bike:      false,
		Climb:     false,
		RopesHigh: false,
		RopesLow:  false,
	}

	// []guides := Guides{
	fmt.Printf("%#v\n", Darby)
	fmt.Printf("%#v\n", Paige)

}
