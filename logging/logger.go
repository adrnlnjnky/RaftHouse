package logg

import (
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

// Logger will log things
func Logger() gin.HandlerFunc {
	return gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		return fmt.Sprintf("%s [%s] %s | %s | %d | %s\n%s\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC822),
			param.Method,
			param.Path,
			param.StatusCode,
			param.Latency,
			param.Keys,
		)
	})
}

// SetupLogOutput sets up any logs that are running
func SetupLogOutput() {
	f, err := os.Create("logging/gin.log")
	if err != nil {
		log.Panic("something went wrong with the Log output setup. Error: ", err)
	}
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	f, err = os.OpenFile("logging/main.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	log.SetOutput(f)
}
