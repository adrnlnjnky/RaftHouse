package equipment

// TrailerBasket represents the front basket of our trailers
type TrailerBasket struct {
	Paddles      Quantity
	Throwbags    Quantity
	DittyBags    Quantity
	WaterBottles Quantity
	WrapKits     Quantity
	Pump         Quantity
	SpareJandH   Quantity
}

func (t *TrailerBasket) PaddlesInTrailer() Quantity {
	return t.Paddles
}
func (t *TrailerBasket) AddPaddlesToTrailer(amount Quantity) {
	t.Paddles += amount
}
func (t *TrailerBasket) RemovePaddlesFromTrailer(amount Quantity) error {
	if amount > t.Paddles {
		return errNotAvailable

	}
	t.Paddles -= amount
	return nil
}

//  ThrowBags in and out of Trailer
func (t *TrailerBasket) ThrowBagsInTrailer() Quantity {
	return t.Throwbags
}
func (t *TrailerBasket) AddThrowBagsToTrailer(amount Quantity) {
	t.Throwbags += amount
}
func (t *TrailerBasket) RemoveThrowBagsFromTrailer(amount Quantity) error {
	if amount > t.Throwbags {
		return errNotAvailable
	}
	t.Throwbags -= amount
	return nil
}

func (t *TrailerBasket) DittyBagsInTrailer() Quantity {
	return t.DittyBags
}
func (t *TrailerBasket) AddDittyBagsToTrailer(amount Quantity) {
	t.DittyBags += amount
}
func (t *TrailerBasket) RemoveDittyBagsFromTrailer(amount Quantity) error {
	if amount > t.DittyBags {
		return errNotAvailable
	}
	t.DittyBags -= amount
	return nil
}

func (t *TrailerBasket) WrapKitsInTrailer() Quantity {
	return t.WrapKits
}
func (t *TrailerBasket) AddWrapKitsToTrailer(amount Quantity) {
	t.WrapKits += amount
}
func (t *TrailerBasket) RemoveWrapKitsFromTrailer(amount Quantity) error {
	if amount > t.WrapKits {
		return errNotAvailable
	}
	t.WrapKits -= amount
	return nil
}

func (t *TrailerBasket) WaterBottlesInTrailer() Quantity {
	return t.WaterBottles
}
func (t *TrailerBasket) AddWaterBottlesToTrailer(amount Quantity) {
	t.WaterBottles += amount
}
func (t *TrailerBasket) RemoveWaterBottlesFromTrailer(amount Quantity) error {
	if amount > t.WaterBottles {
		return errNotAvailable
	}
	t.WaterBottles -= amount
	return nil
}

func (t *TrailerBasket) SpareJandHInTrailer() Quantity {
	return t.SpareJandH

}

func (t *TrailerBasket) AddSpareJandHToTrailer(amount Quantity) {
	t.SpareJandH += amount
}
func (t *TrailerBasket) RemoveSpareJandHFromTrailer(amount Quantity) error {
	if amount > t.SpareJandH {
		return errNotAvailable
	}
	t.SpareJandH -= amount
	return nil
}

func (t *TrailerBasket) PumpInTrailer() Quantity {
	return t.Pump
}
func (t *TrailerBasket) AddPumpToTrailer(amount Quantity) {
	t.Pump += amount
}
func (t *TrailerBasket) RemovePumpFromTrailer(amount Quantity) error {
	if amount > t.Pump {
		return errNotAvailable
	}
	t.Pump -= amount
	return nil
}

type TrailerDeck struct {
	rafts []string
}

func (t *TrailerDeck) BoatsOnTrailer() []string {
	return t.rafts
}
func (t *TrailerDeck) AddRaftsToTrailer(boat string) {
	// choice := RaftPick(GuideChoice)
	choice := "NRS14-1"
	t.rafts = append(t.rafts, choice)
}

func (t *TrailerDeck) RemoveRaftsFromTrailer(boat string) error {
	if len(boat) > len(t.rafts) {
		return errNotAvailable
	}
	return nil
}
