package equipment

import (
	"testing"
)

// func TestReturning(t *testing.T) {
// 		paddles := Paddles{}
// 		paddles.Returning(Quantity())
// 		assertBalance(t, paddles, Quantity(10))
// }

func TestPaddles(t *testing.T) {

	t.Run("Returning", func(t *testing.T) {
		paddles := Paddles{}
		paddles.Returning(Quantity(10))
		assertPaddles(t, paddles, Quantity(10))
	})
	t.Run("CheckingOut  Paddles", func(t *testing.T) {
		paddles := Paddles{Quantity(20)}
		err := paddles.CheckingOut(Quantity(10))
		assertPaddles(t, paddles, Quantity(10))
		assertNoError(t, err)
	})
	t.Run("CheckingOut Not enough Inventory", func(t *testing.T) {
		startingBalance := Quantity(20)
		paddles := Paddles{startingBalance}
		err := paddles.CheckingOut(Quantity(100))

		assertPaddles(t, paddles, startingBalance)
		assertError(t, err, errNotAvailable)
	})
}

func TestThrowBags(t *testing.T) {
	t.Run("Returning", func(t *testing.T) {
		throwBags := ThrowBags{}
		throwBags.Returning(Quantity(10))
		assertThrowBags(t, throwBags, Quantity(10))
	})

	t.Run("CheckingOut ThrowBags", func(t *testing.T) {
		throwBags := ThrowBags{Quantity(20)}
		err := throwBags.CheckingOut(Quantity(10))
		assertThrowBags(t, throwBags, Quantity(10))
		assertNoError(t, err)
	})

	t.Run("CheckingOut not Enough Bags", func(t *testing.T) {
		startingBalance := Quantity(20)

		throwBags := ThrowBags{startingBalance}
		err := throwBags.CheckingOut(Quantity(100))
		assertThrowBags(t, throwBags, Quantity(20))
		assertError(t, err, errNotAvailable)

	})
}

func assertPaddles(t *testing.T, paddles Paddles, want Quantity) {
	t.Helper()
	got := paddles.Available()
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}

func assertThrowBags(t *testing.T, bags ThrowBags, want Quantity) {
	t.Helper()
	got := bags.Available()
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}

func assertNoError(t *testing.T, got error) {
	t.Helper()
	if got != nil {
		t.Fatal("got an error but didn't want one")
	}
}

func assertError(t *testing.T, got error, want error) {
	t.Helper()
	if got == nil {
		t.Fatal("didn't get an error but wanted one")
	}

	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}

}
