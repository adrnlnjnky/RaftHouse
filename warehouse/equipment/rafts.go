package equipment

type Rafts struct {
	nrs14 Quantity

	nrs13  Quantity
	nrs13D Quantity
	rmr12  Quantity
	rmr10  Quantity
}

type NRS14 struct {
	available Quantity
}

type NRS13D struct {
	available Quantity
}

type NRS13 struct {
	available Quantity
}

type RMR12 struct {
	available Quantity
}

type RMR10 struct {
	available Quantity
}

type RMRDucky struct {
	available Quantity
}

type NRSDucky struct {
	available Quantity
}

func (r *NRS14) AvailNRS14() Quantity {
	return r.available
}
func (r *NRS14) ReturnNRS14(rafts Quantity) {
	r.available += rafts
}
func (r *NRS14) TakeNRS14(rafts Quantity) {
	r.available -= rafts
}

func (r *NRS13) AvailNRS13() Quantity {
	return r.available
}
func (r *NRS13) ReturnNRS13(rafts Quantity) {
	r.available += rafts
}
func (r *NRS13) TakeNRS13(rafts Quantity) {
	r.available -= rafts
}

func (r *NRS13D) AvailNRS13D() Quantity {
	return r.available
}
func (r *NRS13D) ReturnNRS13D(rafts Quantity) {
	r.available += rafts
}
func (r *NRS13D) TakeNRS13D(rafts Quantity) {
	r.available -= rafts
}

func (r *RMR12) AvailRMR12() Quantity {
	return r.available
}
func (r *RMR12) ReturnRMR12(rafts Quantity) {
	r.available += rafts
}
func (r *RMR12) TakeRMR12(rafts Quantity) {
	r.available -= rafts
}

func (r *RMR10) AvailRMR10() Quantity {
	return r.available
}
func (r *RMR10) ReturnRMR10(rafts Quantity) {
	r.available += rafts
}
func (r *RMR10) TakeRMR10(rafts Quantity) {
	r.available -= rafts
}

func (r *RMRDucky) AvailRMRDucky() Quantity {
	return r.available
}
func (r *RMRDucky) ReturnRMRDucky(rafts Quantity) {
	r.available += rafts
}
func (r *RMRDucky) TakeRMRDucky(rafts Quantity) {
	r.available -= rafts
}

func (r *NRSDucky) AvailNRSDucky() Quantity {
	return r.available
}
func (r *NRSDucky) ReturnNRSDucky(rafts Quantity) {
	r.available += rafts
}
func (r *NRSDucky) TakeNRSDucky(rafts Quantity) {
	r.available -= rafts
}
