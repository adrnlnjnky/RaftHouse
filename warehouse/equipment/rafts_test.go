package equipment

import (
	// "fmt"
	"testing"
)

func TestReturnRafts(t *testing.T) {

	nrs14 := NRS14{Quantity(10)}
	// nrs13 := NRS13{Quantity(10)}
	// nrs13d := NRS13D{Quantity(10)}
	// rmr12 := RMR12{Quantity(10)}
	// rmr10 := RMR10{Quantity(10)}

	raft := Quantity(2)

	// fmt.Println("NRS14 in Warehouse: ", nrs14)

	nrs14.ReturnNRS14(raft)

	// fmt.Println("NRS14 in Warehouse: ", nrs14)
	nrs14.TakeNRS14(raft)
	nrs14.TakeNRS14(raft)

	// fmt.Println("NRS14 in Warehouse: ", nrs14)
	// want := 14
	// got := NRS14INW()

	// 	if got != want {
	// 		t.Error("got %d, want %d", got, want)
	// 	}

}
