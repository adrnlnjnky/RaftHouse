package equipment

import (
	"errors"
	"fmt"
)

var errNotAvailable = errors.New("cannot Check Out, insufficient equipment")

// RiverGearWorker allows access to the River Gear and Paddle Bins
type RiverGearWorker interface {
	Available() Quantity
	CheckingOut(gear string, amount Quantity)
	Returning(gear string, amount Quantity)
}

// RiverGear is gear used on the River during river trips
type RiverGear struct {
	Paddles
	ThrowBags
}

// Quantity is more clear than int
type Quantity int

func (q Quantity) String() string {
	return fmt.Sprintf("%d ", q)
}

// ThrowBags are bags of rope used in river safety
type ThrowBags struct {
	InWarehouse Quantity
}

// Paddles are the tools we use to move the boats, this keeps track of how many we have in the warehouse
type Paddles struct {
	InWarehouse Quantity
}

// CheckingOut returns the amount of gear available
func (r *RiverGear) CheckingOut(gear string, amount Quantity) {
	switch {
	case gear == "Paddles":
		r.Paddles.InWarehouse -= amount
	case gear == "ThrowBags":
		r.ThrowBags.InWarehouse -= amount
	}
}

// Returning returns the amount of gear available
func (r *RiverGear) Returning(gear string, amount Quantity) {
	switch {
	case gear == "Paddles":
		r.Paddles.InWarehouse += amount
	case gear == "ThrowBags":
		r.ThrowBags.InWarehouse += amount
	}
}

// Available returns the amount of gear available
func (r *RiverGear) Available(gear string) (Quantity, error) {
	switch {
	case gear == "Paddles":
		return r.Paddles.InWarehouse, nil
	case gear == "ThrowBags":
		return r.ThrowBags.InWarehouse, nil
	}
	return 0, errNotAvailable
}

func (p *Paddles) Available() Quantity {
	return p.InWarehouse
}
func (p *Paddles) Returning(amount Quantity) {
	p.InWarehouse += amount
}
func (p *Paddles) CheckingOut(amount Quantity) error {
	if amount > p.InWarehouse {
		return errNotAvailable
	}
	p.InWarehouse -= amount
	return nil
}

// Checking ThrowBags in and out
func (t *ThrowBags) Available() Quantity {
	return t.InWarehouse
}
func (t *ThrowBags) Returning(amount Quantity) {
	t.InWarehouse += amount
}
func (t *ThrowBags) CheckingOut(amount Quantity) error {
	if amount > t.InWarehouse {
		return errNotAvailable
	}
	t.InWarehouse -= amount
	return nil
}

type DittyBags struct {
	InWarehouse Quantity
}

func (p *DittyBags) Available() Quantity {
	return p.InWarehouse
}
func (p *DittyBags) Returning(amount Quantity) {
	p.InWarehouse += amount
}
func (w *DittyBags) CheckingOut(amount Quantity) error {
	if amount > w.InWarehouse {
		return errNotAvailable
	}
	w.InWarehouse -= amount
	return nil
}

type WaterBottles struct {
	InWarehouse Quantity
}

func (p *WaterBottles) Available() Quantity {
	return p.InWarehouse
}
func (p *WaterBottles) Returning(amount Quantity) {
	p.InWarehouse += amount
}
func (w *WaterBottles) CheckingOut(amount Quantity) error {
	if amount > w.InWarehouse {
		return errNotAvailable
	}
	w.InWarehouse -= amount
	return nil
}

type WrapKits struct {
	InWarehouse Quantity
}

func (p *WrapKits) Available() Quantity {
	return p.InWarehouse
}
func (p *WrapKits) Returning(amount Quantity) {
	p.InWarehouse += amount
}
func (w *WrapKits) CheckingOut(amount Quantity) error {
	if amount > w.InWarehouse {
		return errNotAvailable
	}
	w.InWarehouse -= amount
	return nil
}

type Pump struct {
	InWarehouse Quantity
}

func (p *Pump) Available() Quantity {
	return p.InWarehouse
}
func (p *Pump) Returning(amount Quantity) {
	p.InWarehouse += amount
}
func (w *Pump) CheckingOut(amount Quantity) error {
	if amount > w.InWarehouse {
		return errNotAvailable
	}
	w.InWarehouse -= amount
	return nil
}

type SpareJandH struct {
	InWarehouse Quantity
}

func (p *SpareJandH) Available() Quantity {
	return p.InWarehouse
}
func (p *SpareJandH) Returning(amount Quantity) {
	p.InWarehouse += amount
}
func (w *SpareJandH) CheckingOut(amount Quantity) error {
	if amount > w.InWarehouse {
		return errNotAvailable
	}
	w.InWarehouse -= amount
	return nil
}

// func GrabGear(stuff RiverGear, amount Quantity) {
// 	add(stuff, amount)
// 	// checkout :=
// }

// func add(stuff RiverGear, amount Quantity) Quantity {
// 	gear := RiverGear(stuff)
// 	// return gear.InWarehouse - amount
// 	fmt.Println(gear)
// 	return RiverGear(stuff) - amount
// }

// checkin Paddles in and ou
