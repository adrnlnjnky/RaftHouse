package servers

import (
	"github.com/gin-gonic/gin"
	gindump "github.com/tpkeeper/gin-dump"
	"gitlab.com/adrnlnjnky/RaftHouse/helpers"
	router "gitlab.com/adrnlnjnky/RaftHouse/routers"
)

//Central is the main server that launches the website
func Central() {
	// These are helper variables to make the code easier to read
	view := router.View
	checkIn := router.CheckIn
	checkOut := router.CheckOut
	add := router.Add
	remove := router.Remove

	// starting our gin framework server with a custom logger function and recovery, then setting up the html and css
	// templates.  Below that is the initial routing links that use the variables above.
	server := gin.New()
	server.Use(
		gin.Recovery(),
		gin.Logger(),
		gindump.Dump(),
	)
	server.Static("/CSS", "./templates/css")
	server.LoadHTMLGlob("templates/*.tmpl")

	// This group interfaces gear for the guides to take and return
	// guideRoute := server.Group("/guide", authorize.BasicAuth())
	guideRoute := server.Group("/guide")
	{
		go guideRoute.GET("/gear/:search", func(ctx *gin.Context) {
			view(ctx)
		})
		go guideRoute.PUT("/return", func(ctx *gin.Context) {
			checkIn(ctx)
		})
		go guideRoute.PATCH("/return", func(ctx *gin.Context) {
			checkIn(ctx)
		})

		go guideRoute.PUT("/checkout", func(ctx *gin.Context) {
			checkOut(ctx)
		})
		go guideRoute.PATCH("/checkout", func(ctx *gin.Context) {
			checkOut(ctx)
		})

	}

	// there routes are for adding inventory and removing broken equipment.
	managerRoute := server.Group("/manage")
	{
		managerRoute.POST("/add", func(ctx *gin.Context) {
			add(ctx)
		})
		managerRoute.DELETE("/remove", func(ctx *gin.Context) {
			remove(ctx)
		})
	}

	// This route launches the website
	// viewRoute := server.Group("/", authorize.BasicAuth())
	viewRoute := server.Group("/")
	{
		viewRoute.GET("/warehouse", router.ViewEquip)
		viewRoute.GET("/home", func(ctx *gin.Context) {
			// server.GET("/home", authorize.BasicAuth() {
			ctx.HTML(200, "index.tmpl", "test")
		})

		viewRoute.GET("/ws", func(ctx *gin.Context) {
			router.PushStuff(ctx.Writer, ctx.Request)
		})
	}

	port, route := helpers.SetupServerEnv(".env")
	if route == "" {
		route = "localhost:"
	}
	if port == "" {
		port = "7070"
	}

	server.Run(route + ":" + port)

}
