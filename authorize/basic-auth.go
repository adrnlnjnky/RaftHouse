package authorize

import "github.com/gin-gonic/gin"

// BasicAuth is for authorizing
func BasicAuth() gin.HandlerFunc {
	return gin.BasicAuth(gin.Accounts{
		"adrnlnjnky": "password",
	})
}
