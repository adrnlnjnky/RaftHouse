module gitlab.com/adrnlnjnky/RaftHouse

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/terraform-plugin-sdk v1.16.0
	github.com/joho/godotenv v1.3.0
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/mgutz/logxi v0.0.0-20161027140823-aebf8a7d67ab
	github.com/tpkeeper/gin-dump v1.0.0
	gorm.io/driver/postgres v1.0.6
	gorm.io/gorm v1.20.9
)
